<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    // поля, заполняемые в таблице 
    protected $fillable = [ 
    	'slug', 
    	'title', 
    	'body', 
    	'category_id'
    ]; 
}
