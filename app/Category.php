<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    // поля, заполняемые в таблице 
    protected $fillable = [ 
    	'slug', 
    	'title' 
    ]; 
}
